### Minimum Requirement
```
NODE 6.0
NPM 5.0
```

### Installation
```
npm install
npm install nodemon -g
```

### Debugging
```
export DEBUG=GS:*
```

### Export variables
```
export PORT=3006
export DEBUG=GS:*
export export GS_DB_URL=mongodb://127.0.0.1:27017/shipment
```

### Run the Application (Development Purpose Only)
```
npm run dev
```


### Prepare build
```
npm run build
```


### Run the Application (Production Purpose Only)
```
npm start
```