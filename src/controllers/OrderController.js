import Debug from 'debug'
import _ from 'lodash';
import Boom from 'boom';

const debug = Debug('GS:OrderController');
const UserController = {};

UserController.orderDetails = (req, res, next) => {
    if (_.isEmpty(req.orderStore)) {
        return next(new Boom.notFound('Please try again'))
    }
    req.data = req.orderStore;
    return next();
};

UserController.orders = (req, res, next) => {
    if (_.isEmpty(req.orderStore)) {
        return next(new Boom.notFound('Their is no order'))
    }
    req.data = req.orderStore;
    return next();
};

export default UserController;