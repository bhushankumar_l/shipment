import Debug from 'debug'
import express from 'express';
import bodyParser from 'body-parser';
import session from 'express-session';
import config from './config';
import routes from './routes';
import  './config/db/index';

let app = express();
const debug = Debug('GS:App');

app.use(session({
    secret: 'THIS_IS_SECRET',
    cookie: {
        maxAge: 1000000
    },
    resave: true,
    saveUninitialized: true
}));

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({extended: false}));

/**
 *  This is our route middleware
 */
app.use('/api/v1', routes);

/**
 *  Error handling
 */
app.use(config.handleError);

/**
 *  Handle response
 */
app.use(config.handleSuccess);

app.set('PORT', process.env.PORT || 3006);
app.listen(app.get('PORT'));

debug('Server has been started on PORT %o ', app.get('PORT'));

export default app;