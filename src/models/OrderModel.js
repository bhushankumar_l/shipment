import mongoose from 'mongoose';
let Schema = mongoose.Schema;

let orderSchema = new Schema({
    _id: {type: String},
    companyName: {type: String},
    customerAddress: {type: String},
    orderedItem: {type: String}
}, {timestamps: true});

let OrderModel = mongoose.model('order', orderSchema, 'order');

export default OrderModel;
