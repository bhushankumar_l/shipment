import Debug from 'debug';

const debug = Debug('GS:Config');
const config = {};

let _ = require('lodash');

config.handleError = (err, req, res, next) => {
    if (!err) {
        return next();
    }
    let errorResponse = {
        error: _.merge({
            stack: err.stack
        }, err.output && err.output.payload ? err.output.payload : err)
    };
    debug('Error stack :: ');
    debug('----------------------------------------------------------------------------------- ');
    return res.status(err.output && err.output.statusCode ? err.output.statusCode : 500).json(errorResponse);
};

config.handleSuccess = (req, res, next) => {
    if (req.data === undefined) {
        debug('Return from undefined req.data ');
        return next();
    }

    let resObject = req.data || [];
    debug('Success response :: ');
    debug('----------------------------------------------------------------------------------- ');
    return res.json(resObject);
};

export default config;