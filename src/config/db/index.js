import Debug from 'debug';
import mongoose from  'mongoose';
import {DB} from '../../constants/';
import bluebird from 'bluebird';

const debug = Debug('GS:DB');

mongoose.Promise = bluebird;

mongoose.connect(DB.URL, {server: {socketOptions: {keepAlive: 1}}}).then(() => {
    debug('connected ', DB.URL);
}).catch((error) => {
    debug('error', error);
});

export default mongoose;