import express from 'express';
import OrderService from '../services/OrderService';
import OrderController from '../controllers/OrderController';

let router = express.Router();

/**
 *  Create order
 */
router.post('/', [
    OrderService.validateOrderParams,
    OrderService.create,
    OrderController.orderDetails
]);

/**
 *  Delete order by id
 */
router.delete('/:id', [
    OrderService.validateOrderId,
    OrderService.searchOneById,
    OrderService.removeOrder,
    OrderController.orders
]);

/**
 *  Show order by company
 */
router.get('/company', [
    OrderService.validateCompany,
    OrderService.searchOrdersByCompany,
    OrderController.orders
]);

/**
 *  Show order by address
 */
router.get('/address', [
    OrderService.validateAddress,
    OrderService.searchOrdersByAddress,
    OrderController.orders
]);

/**
 *  Display order by number of purchased
 */
router.get('/', [
    OrderService.displayOrders,
    OrderController.orders
]);

export default router;