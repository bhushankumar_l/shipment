import express from 'express';
import OrderRoute from './OrderRoute';

let router = express.Router();

router.use('/order', OrderRoute);

export default router;