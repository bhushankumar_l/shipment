import Debug from 'debug'
import Boom from 'boom';
import _ from  'lodash';
import OrderModel from '../models/OrderModel';

const debug = Debug('GS:OrderService');

let OrderService = {};

OrderService.validateOrderParams = (req, res, next) => {
    let params = _.merge(req.params, req.body);
    if (_.isEmpty(params._id)) {
        return next(new Boom.notFound('Please Enter _id'));
    } else if (_.isEmpty(params.orderedItem)) {
        return next(new Boom.notFound('Please Enter orderedItems'));
    } else if (_.isEmpty(params.companyName)) {
        return next(new Boom.notFound('Please enter companyName'));
    } else if (_.isEmpty(params.customerAddress)) {
        return next(new Boom.notFound('Please enter customerAddress'));
    }
    return next();
};

OrderService.create = async (req, res, next) => {
    let params = _.merge(req.params, req.body);
    let data = {
        _id: params._id,
        orderedItem: params.orderedItem,
        companyName: params.companyName,
        customerAddress: params.customerAddress
    };
    req.orderStore = await OrderModel.create(data).catch((error) => {
        debug('error:%o', error);
        return next(error);
    });
    return next();
};

OrderService.validateCompany = (req, res, next) => {
    let params = req.query;
    if (_.isEmpty(params.companyName)) {
        return next(new Boom.notFound('Please enter companyName'));
    }
    return next();
};

OrderService.searchOrdersByCompany = async (req, res, next) => {
    let params = req.query;
    let data = {
        companyName: params.companyName
    };
    req.orderStore = await OrderModel.find(data).catch((error) => {
        debug('error:%o', error);
        return next(error);
    });
    return next();
};

OrderService.validateAddress = (req, res, next) => {
    let params = req.query;
    if (_.isEmpty(params.customerAddress)) {
        return next(new Boom.notFound('Please enter customerAddress'));
    }
    return next();
};

OrderService.searchOrdersByAddress = async (req, res, next) => {
    let params = req.query;
    let data = {
        customerAddress: params.customerAddress
    };
    //debug('data:%o', data);
    req.orderStore = await OrderModel.find(data).catch((error) => {
        debug('error:%o', error);
        return next(error);
    });
    return next();
};

OrderService.validateOrderId = (req, res, next) => {
    let params = _.merge(req.params, req.body);
    if (_.isEmpty(params.id)) {
        return next(new Boom.notFound('Please enter id'));
    }
    return next();
};

OrderService.searchOneById = async (req, res, next) => {
    let params = _.merge(req.params, req.body);
    let data = {
        _id: params.id
    };
    //debug('data:%o', data);
    req.orderStore = await OrderModel.find(data).catch((error) => {
        debug('error:%o', error);
        return next(error);
    });
    return next();
};

OrderService.removeOrder = async (req, res, next) => {
    if (_.isEmpty(req.orderStore)) {
        return next();
    }
    let params = _.merge(req.params, req.body);
    let data = {
        _id: params.id
    };
    //debug('data:%o', data);
    await OrderModel.remove(data).catch((error) => {
        debug('error:%o', error);
        return next(error);
    });
    req.orderStore = {message: 'Removed successfully'};
    return next();
};

OrderService.displayOrders = async (req, res, next) => {
    OrderModel.aggregate([
        {'$group': {_id: '$orderedItem', count: {$sum: 1}}},
        {$sort: {'count': -1}}
    ]).exec(function (error, results) {
        if (error) {
            debug('error:%o', error);
            return next(error);
        }
        req.orderStore = results;
        return next();
    })
};

export default OrderService;